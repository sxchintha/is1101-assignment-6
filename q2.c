#include <stdio.h>

int fibonacciSeq(int n);
int nextNum(int n);
int next = 0;

int fibonacciSeq(int n)
{
	if(n>=0&&next<=n)
	{
		printf("%d\n", nextNum(next));
		next++;
		fibonacciSeq(n);
	}
}

int nextNum(int n)
{
	if(n<=1) return n;
	else return nextNum(n-1)+nextNum(n-2);
}

int main()
{
	int n;
	printf("Please enter a number: ");
	scanf("%d", &n);

	fibonacciSeq(n);

	return 0;
}
