#include <stdio.h>

void pattern(int n);
void nextNum(int n);
int row = 1;

void nextNum(int n)
{
	if (n>0)
	{
		printf("%d", n);
		nextNum(n-1);
	}
}

void pattern(int n)
{
	if(n>0)
	{
		nextNum(row);
		printf("\n");
		row++;
		pattern(n-1);
	}
}

int main()
{
	int n;
	printf("Please enter a number: ");
	scanf("%d", &n);

	pattern(n);

	return 0;
}
